#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
	  exec startx
fi

alias ls='exa -la --group-directories-first --icons'
alias lt='exa -a --tree'

PS1='[\u@\h \W]\$ '
. "$HOME/.cargo/env"
export GPG_TTY=$(tty)
export EDITOR=vim
export PATH="${HOME}/.emacs.d/bin:${PATH}"
